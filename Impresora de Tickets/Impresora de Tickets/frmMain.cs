﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Impresora_de_Tickets
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Función que calcula los kilogramos del peso a partir del valor en libras.
        /// </summary>
        /// <param name="Libras">Valor del peso en libras.</param>
        /// <returns>Retorna un Double con el peso en Kilogramos.</returns>
        public double lb2Kg(double Libras)
        {
            return Libras / 2.20462;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            double IMC = lb2Kg ( (double) nudPeso.Value) / Math.Pow((double)nudAltura.Value/100, 2);
            txtIMC.Text = IMC.ToString("0.000");

        }

        private void nudPeso_ValueChanged(object sender, EventArgs e)
        {
            double IMC = lb2Kg((double)nudPeso.Value) / Math.Pow((double)nudAltura.Value / 100, 2);
            txtIMC.Text = IMC.ToString("0.000");
        }

        private void btnHTMLCrear_Click(object sender, EventArgs e)
        {
            try
            {
                string FormatoHTML = Properties.Resources.Formato;
                string w3 = Properties.Resources.w3;

                string curDir = Directory.GetCurrentDirectory();
                File.WriteAllText(curDir + "/W3.css", w3);

                string HTML_New = FormatoHTML.Replace("##VALUE01##", lblTime.Text);
                HTML_New = HTML_New.Replace("##VALUE02##", txtNombre.Text);
                HTML_New = HTML_New.Replace("##VALUE03##", nudPeso.Text);
                HTML_New = HTML_New.Replace("##VALUE04##", nudAltura.Text);
                HTML_New = HTML_New.Replace("##VALUE05##", nudEdad.Text);
                HTML_New = HTML_New.Replace("##VALUE06##", txtIMC.Text);

                File.WriteAllText(curDir + "/PrintPage.html", HTML_New);

                webBrowser1.Url = new Uri(curDir + "/PrintPage.html");
            }
            catch (Exception ex)
            {
                // Get reference to the dialog type.
                var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);

                // Create dialog instance.
                var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());

                // Populate relevant properties on the dialog instance.
                dialog.Text = "Error al crear archvios";
                dialogType.GetProperty("Details").SetValue(dialog, ex.StackTrace, null);
                dialogType.GetProperty("Message").SetValue(dialog, ex.Message, null);

                // Display dialog.
                var result = dialog.ShowDialog();
            }
        }

        private void btnPrintDialog_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPrintPreviewDialog();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            webBrowser1.Print();
            string curDir = Directory.GetCurrentDirectory();
            string path = Directory.GetCurrentDirectory() + "/log.csv";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("TimeStamp, Nombre, Peso, Altura, Edad, IMC");
                }
            }
            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine("{0},{1},{2},{3},{4},{5}",
                        lblTime.Text, txtNombre.Text, nudPeso.Text,
                        nudAltura.Text, nudEdad.Text, txtIMC.Text);
            }

        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
